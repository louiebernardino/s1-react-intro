// 1. We want to import dependencies
import React, {Fragment} from 'react'
import ReactDOM from 'react-dom'
// 2. we need to define the root
const root = document.querySelector("#root")
// 3. create component
const pageComponent = (
	<Fragment>
		<h1>Hello, World!</h1>
		<button>Save!</button>

	</Fragment>


	)
// 4. render component
ReactDOM.render(pageComponent, root)